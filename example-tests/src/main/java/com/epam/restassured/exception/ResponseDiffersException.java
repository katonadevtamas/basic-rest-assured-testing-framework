package com.epam.restassured.exception;

/**
 * Exception class for handling differs in actual response from the expected response, and by catching these exceptions
 * the real threshold of the actual test is measurable and comparable to the expected value.
 *
 * Created by Tamas_Katona1 on 07/19/2016.
 */
public class ResponseDiffersException extends Exception {

    public ResponseDiffersException(String errorMessage) { super(errorMessage); }

    public ResponseDiffersException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }
}
