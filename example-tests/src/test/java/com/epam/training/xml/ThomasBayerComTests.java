package com.epam.training.xml;

import com.epam.training.common.TestClassesCommon;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
/**
 * Class for testing API of the public REST-ful service can be found at http://www.thomas-bayer.com/sqlrest/ .
 * Created by Tamas_Katona1 on 2016-11-11.
 */
public class ThomasBayerComTests extends TestClassesCommon {

    private static final Logger LOG = LogManager.getLogger(ThomasBayerComTests.class);
    private static final String API_URL = THOMAS_BAYER_COM_ENVIRONMENT_URL;

    private void debug(String text) {
        if (LOG_TESTS)
            LOG.debug(text);
    }

    private void info(String text) {
        if (LOG_TESTS)
            LOG.info(text);
    }

    private void checkProductById(int id, int expectedHTTPStatusCode) {
        debug("Response for get(\"" + API_URL + "/PRODUCT/" + id + "\"):\n"
                + when().get(API_URL + "/PRODUCT/" + id).asString());

        when()
                .get(API_URL + "/PRODUCT/" + id)
                .then()
                .statusCode(expectedHTTPStatusCode);
    }

    private void putProductById(int id, String xmlAsString, int expectedHTTPStatusCode) {
        given()
                .contentType("application/xml")
                .body(xmlAsString)
                .when()
                .put(API_URL + "/PRODUCT/" + id)
                .then()
                .statusCode(expectedHTTPStatusCode);
    }

    private void postProductById(String xmlAsString, int expectedHTTPStatusCode) {
        given().
                contentType("application/xml").
                body(xmlAsString).
                when().post(API_URL + "/PRODUCT").
                then().statusCode(expectedHTTPStatusCode);
    }

    private void deleteProductById(int id, int expectedHTTPStatusCode) {
        when().
                delete(API_URL + "/PRODUCT/" + id).
                then().
                statusCode(expectedHTTPStatusCode);
    }

    private String xmlStringForPutProduct(int productId) {
        return "<resource>\n" +
                "\t<NAME>REST-Assured Test " + productId + "</NAME>\n" +
                "\t<PRICE>" + productId + "." + productId + "</PRICE>\n" +
                "</resource>";
    }

    private String xmlStringForPostProduct(int productId) {
        return "<resource>\n"
                + "\t<ID>" + productId + "</ID>\n" +
                "\t<NAME>REST-Assured Test " + productId + "</NAME>\n" +
                "\t<PRICE>" + productId + "." + productId + "</PRICE>\n" +
                "</resource>";
    }

    public void doSg() {

    }

    @Ignore
    @Test
    public void testExistingProducts() {
        checkProductById(0, HTTP_STATUS_CODE_OK);
        checkProductById(1, HTTP_STATUS_CODE_OK);
        checkProductById(2, HTTP_STATUS_CODE_OK);
    }

    @Ignore
    @Test
    public void testNonExistingProducts() {
        checkProductById(-5, HTTP_STATUS_CODE_NOT_FOUND);
        checkProductById(666, HTTP_STATUS_CODE_NOT_FOUND);
        checkProductById(6 - 9 * 2, HTTP_STATUS_CODE_NOT_FOUND);
    }

    @Ignore
    @Test
    public void testPutProduct() {
        int productId = 1000;

        // 1. Check whether the product exists or not. It should not exist.
        checkProductById(productId, HTTP_STATUS_CODE_NOT_FOUND);

        // 2. Create the input XML will be used for the PUT request.
        String xmlAsString = xmlStringForPutProduct(productId);

        // 3. Send the PUT request to the API.
        putProductById(productId, xmlAsString, HTTP_STATUS_CODE_CREATED);
    }

    @Ignore
    @Test
    public void testPostProduct() {
        int productId = 1000;

        // 1. Check whether the product exists or not. It should not exist.
        checkProductById(productId, HTTP_STATUS_CODE_NOT_FOUND);

        // 2. Create the input XML will be used for the POST request.
        String xmlAsString = xmlStringForPostProduct(productId);

        // 3. Send the POST request to the API.
        postProductById(xmlAsString, HTTP_STATUS_CODE_CREATED);
    }

    @Ignore
    @Test
    public void testDeleteProduct() {
        int productId = 1000;

        // 1. Check whether the product exists or not. It should exist.
        checkProductById(productId, HTTP_STATUS_CODE_OK);

        // 2. Send the DELETE request to the API.
        deleteProductById(productId, HTTP_STATUS_CODE_OK);
    }

    @Test
    public void testE2E() {
        int productId = 2000;

        // 1. Check whether the product exists or not. It shouldn't exist at this point.
        checkProductById(productId, HTTP_STATUS_CODE_NOT_FOUND);

        // 2. Create the input XML will be used for the POST request.
        String xmlAsString = xmlStringForPostProduct(productId);

        // 3. Send the POST request to the API.
        postProductById(xmlAsString, HTTP_STATUS_CODE_CREATED);

        // 4. Check whether the product exists or not. It should exist at this point.
        checkProductById(productId, HTTP_STATUS_CODE_OK);

        // 5. Send the DELETE request to the API.
        deleteProductById(productId, HTTP_STATUS_CODE_OK);

        // 6. Check whether the product exists or not. It shouldn't exist at this point.
        checkProductById(productId, HTTP_STATUS_CODE_NOT_FOUND);

        // 7. Create the input XML will be used for the PUT request.
        xmlAsString = xmlStringForPutProduct(productId);

        // 8. Send the PUT request to the API.
        putProductById(productId, xmlAsString, HTTP_STATUS_CODE_CREATED);

        // 9. Check whether the product exists or not. It should exist at this point.
        checkProductById(productId, HTTP_STATUS_CODE_OK);

        // 10. Send the DELETE request to the API.
        deleteProductById(productId, HTTP_STATUS_CODE_OK);

        // 11. Check whether the product exists or not. It shouldn't exist at this point.
        checkProductById(productId, HTTP_STATUS_CODE_NOT_FOUND);
    }
}
