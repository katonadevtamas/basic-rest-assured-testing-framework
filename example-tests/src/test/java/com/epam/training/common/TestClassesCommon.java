package com.epam.training.common;

import com.epam.restassured.env.EnvironmentProvider;

import java.util.Map;

/**
 * Class for common functions, methods, constants using by API testing related classes.
 * Created by Tamas_Katona1 on 2016-11-11.
 */
public abstract class TestClassesCommon {

    protected static EnvironmentProvider environmentProvider = new EnvironmentProvider();
    protected static Map<String, String> envProperties = environmentProvider.get();

    protected static final String LOG_TESTS_PROPERTY = "LOG_TESTS";
    protected static final String THOMAS_BAYER_COM_ENVIRONMENT_URL_PROPERTY = "THOMAS_BAYER_COM_ENVIRONMENT_URL";

    protected static final boolean LOG_TESTS = Boolean.parseBoolean(envProperties.get(LOG_TESTS_PROPERTY));
    protected static final String THOMAS_BAYER_COM_ENVIRONMENT_URL = envProperties.get(THOMAS_BAYER_COM_ENVIRONMENT_URL_PROPERTY);


    // HTML Status codes
    protected static final int HTTP_STATUS_CODE_OK = 200;
    protected static final int HTTP_STATUS_CODE_CREATED = 201;
    protected static final int HTTP_STATUS_CODE_BAD_REQUEST = 400;
    protected static final int HTTP_STATUS_CODE_NOT_FOUND = 404;
    protected static final int HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR = 500;

}
